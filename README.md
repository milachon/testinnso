# TestInnso

## Prerequisites

* Need Java >8

## Build

* execute command "./gradlew build" or "gradlew.bat build" with Windows
* resulting jar is available in ./build/libs/

## Launch
Execute command: java -jar test_innso-0.1.0.jar [--server.port=8080]

## Exposed API 
Auto generated api doc: 
http://localhost:8080/swagger-ui.html



