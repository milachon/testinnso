# TestInnso Test plan

You can use the swagger documentation tool  (<url>/swagger-ui.html) to execute request or software like curl or postman. 
Here only curl call will be defined. 

## Step1

# Cr�ation d�un message de la part de ��J�r�mie�Durand��, avec le contenu suivant�: ��Bonjour, j�ai un probl�me avec mon nouveau t�l�phone��

* Execute command : 
curl -X POST "http://localhost:8080/messages" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"author\": \"J�r�mie Durand\", \"content\": \"Bonjour, j�ai un probl�me avec mon nouveau t�l�phone\"}"

* Attempt result : 
HTTP 201
{
  "uuid": "{uuid_mess1}",
  "createdAt": "2021-03-02 04:09:16", // with current GMT date respecting this format
  "author": "J�r�mie Durand",
  "content": "Bonjour, j�ai un probl�me avec mon nouveau t�l�phone"
}

* Keep the uuid_mess1 value.

## Step2

# Cr�ation d�un dossier client, avec pour nom du client ��J�r�mie Durand��, et avec le message pr�c�dent dans la liste

* Execute command : 
curl -X POST "http://localhost:8080/customer_folders/create_from_message/{uuid_mess1}" -H "accept: */*" with uuid_mess1 replaced by value stored at Step1.

* Attempt result: 
HTTP 201
{
  "uuid": "{uuid_folder1}",
  "createdAt": "2021-03-02 04:12:27", // with current GMT date respecting this format
  "customerName": "J�r�mie Durand",
  "messages": [
    {
      "uuid": "{uuid_mess1}",
      "createdAt": "2021-03-02 04:09:16",
      "author": "J�r�mie Durand",
      "content": "Bonjour, j�ai un probl�me avec mon nouveau t�l�phone",
      "customerFolder": "{uuid_folder1}"
    }
  ]
}

* Keep the uuid_folder1 value.

## Step3

# Cr�ation d�un message de la part de ��Sonia Valentin��, avec le contenu suivant�: ��Je suis Sonia, et je vais mettre tout en �uvre pour vous aider. Quel est le mod�le de votre t�l�phone�?���; ce message sera rattach� au dossier client pr�c�dent

* Execute command replacing {uuid_folder1} : 
curl -X POST "http://localhost:8080/customer_folders/{uuid_folder1}/messages" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"author\": \"Sonia Valentin\", \"content\": \"Je suis Sonia, et je vais mettre tout en �uvre pour vous aider. Quel est le mod�le de votre t�l�phone�?\"}"

* Attempt result : 
HTTP 201
{
  "uuid": "{uuid_folder1}",
  "createdAt": "2021-03-02 04:12:27",
  "customerName": "J�r�mie Durand",
  "messages": [
    {
      "uuid": "{uuid_mess1}",
      "createdAt": "2021-03-02 04:09:16",
      "author": "J�r�mie Durand",
      "content": "Bonjour, j�ai un probl�me avec mon nouveau t�l�phone",
      "customerFolder": "{uuid_folder1}"
    },
    {
      "uuid": "{uuid_mess2}",
      "createdAt": "2021-03-02 04:15:39",
      "author": "Sonia Valentin",
      "content": "Je suis Sonia, et je vais mettre tout en �uvre pour vous aider. Quel est le mod�le de votre t�l�phone ?",
      "customerFolder": "{uuid_folder1}"
    }
  ]
}

## Step4 

# Modification du dossier client en ajoutant la r�f�rence client�: ��KA-18B6��. Cela permet de valider l�API qui modifie un dossier client.

* Execute command replacing {uuid_folder1}: 
curl -X PUT "http://localhost:8080/customer_folders/{uuid_folder1}" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"reference\": \"KA-18B6\"}"

* Attempt result : 
HTTP 200
{
  "uuid": "{uuid_folder1}",
  "createdAt": "2021-03-02 04:12:27",
  "customerName": "J�r�mie Durand",
  "reference": "KA-18B6",
  "messages": [
    {
      "uuid": "{uuid_mess1}",
      "createdAt": "2021-03-02 04:09:16",
      "author": "J�r�mie Durand",
      "content": "Bonjour, j�ai un probl�me avec mon nouveau t�l�phone",
      "customerFolder": "{uuid_folder1}"
    },
    {
      "uuid": "{uuid_mess2}",
      "createdAt": "2021-03-02 04:15:39",
      "author": "Sonia Valentin",
      "content": "Je suis Sonia, et je vais mettre tout en �uvre pour vous aider. Quel est le mod�le de votre t�l�phone ?",
      "customerFolder": "{uuid_folder1}"
    }
  ]
}


## Step5

# R�cup�ration de tous les dossiers clients actuels. Le r�sultat contient juste un dossier client, celui pr�c�demment cr��.

* Execute command : curl -X GET "http://localhost:8080/customer_folders" -H "accept: */*"

* Attempt result : 
HTTP 200
{
  "content": [
    {
      "uuid": "{uuid_folder1}",
      "createdAt": "2021-03-02 04:12:27",
      "customerName": "J�r�mie Durand",
      "reference": "KA-18B6",
      "messages": [
        {
          "uuid": "{uuid_mess1}",
          "createdAt": "2021-03-02 04:09:16",
          "author": "J�r�mie Durand",
          "content": "Bonjour, j�ai un probl�me avec mon nouveau t�l�phone",
          "customerFolder": "{uuid_folder1}"
        },
        {
          "uuid": "{uuid_mess2}",
          "createdAt": "2021-03-02 04:15:39",
          "author": "Sonia Valentin",
          "content": "Je suis Sonia, et je vais mettre tout en �uvre pour vous aider. Quel est le mod�le de votre t�l�phone ?",
          "customerFolder": "{uuid_folder1}"
        }
      ]
    }
  ],
  "pageable": {
    "sort": {
      "sorted": false,
      "unsorted": true,
      "empty": true
    },
    "offset": 0,
    "pageNumber": 0,
    "pageSize": 20,
    "paged": true,
    "unpaged": false
  },
  "totalElements": 1,
  "totalPages": 1,
  "last": true,
  "size": 20,
  "number": 0,
  "sort": {
    "sorted": false,
    "unsorted": true,
    "empty": true
  },
  "numberOfElements": 1,
  "first": true,
  "empty": false
}
