package com.milachon.TestInnso.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.milachon.TestInnso.domain.model.Message;
import com.milachon.TestInnso.manager.MessageManager;

@RestController
@RequestMapping("/messages")
public class MessageRestController extends AbstractRestController<MessageManager, Message> {
}
