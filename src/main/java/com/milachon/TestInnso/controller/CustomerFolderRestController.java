package com.milachon.TestInnso.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.milachon.TestInnso.domain.model.CustomerFolder;
import com.milachon.TestInnso.domain.model.Message;
import com.milachon.TestInnso.manager.CustomerFolderManager;

@RestController
@RequestMapping("/customer_folders")
public class CustomerFolderRestController
    extends AbstractRestController<CustomerFolderManager, CustomerFolder> {

  @PostMapping(value = "/create_from_message/{message_uuid}")
  public ResponseEntity<CustomerFolder> createFromMessage(@PathVariable String message_uuid) {
    return new ResponseEntity<>(manager.createFromMessage(message_uuid), HttpStatus.CREATED);
  }

  @PostMapping(value = "/{uuid}/messages")
  public ResponseEntity<CustomerFolder> addNewMessage(@PathVariable String uuid,
      @RequestBody(required = true) Message body) {
    return new ResponseEntity<>(manager.addNewMessage(uuid, body), HttpStatus.CREATED);
  }
}
