package com.milachon.TestInnso.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.milachon.TestInnso.manager.ICRUDManager;

@RestController
public abstract class AbstractRestController<T extends ICRUDManager<E>, E> {

  @Autowired
  protected T manager;

  @GetMapping(value = "")
  public Page<E> getList(Pageable pageable) {
    return manager.getList(pageable);
  }

  @GetMapping(value = "/{uuid}")
  public ResponseEntity<E> get(@PathVariable String uuid) {
    return new ResponseEntity<>(manager.getByUuid(uuid), HttpStatus.OK);
  }

  @PostMapping(value = "")
  public ResponseEntity<E> save(@RequestBody(required = true) E body) {
    return new ResponseEntity<>(manager.save(body), HttpStatus.CREATED);
  }

  @PutMapping(value = "/{uuid}")
  public ResponseEntity<E> update(@PathVariable String uuid, @RequestBody(required = true) E body) {
    return new ResponseEntity<>(manager.update(uuid, body), HttpStatus.OK);
  }

  @DeleteMapping(value = "/{uuid}")
  public void delete(@PathVariable String uuid) {
    manager.deleteByUuid(uuid);
  }
}
