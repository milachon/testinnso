package com.milachon.TestInnso.repository;

import java.util.Optional;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

@NoRepositoryBean
public interface IAbstractRepository<T> extends PagingAndSortingRepository<T, String> {

  public Optional<T> findByUuid(String uuid);

  @Transactional
  public void deleteByUuid(String uuid);
}
