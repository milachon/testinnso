package com.milachon.TestInnso.repository;

import com.milachon.TestInnso.domain.model.Message;

public interface IMessageRepository extends IAbstractRepository<Message> {

}
