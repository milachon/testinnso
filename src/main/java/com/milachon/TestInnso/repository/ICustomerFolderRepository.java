package com.milachon.TestInnso.repository;

import com.milachon.TestInnso.domain.model.CustomerFolder;

public interface ICustomerFolderRepository extends IAbstractRepository<CustomerFolder> {

}
