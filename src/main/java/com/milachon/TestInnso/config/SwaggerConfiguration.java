package com.milachon.TestInnso.config;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration.class)
public class SwaggerConfiguration {

  public static final String DEFAULT_INCLUDE_PATTERN = "/.*";
  private final Logger log = LoggerFactory.getLogger(SwaggerConfiguration.class);

  @Bean
  public Docket swaggerSpringfoxDocket() {
    log.debug("Starting Swagger");
    Contact contact = new Contact("Fabien Milachon", "", "milachon@gmail.com");

    List<VendorExtension> vext = new ArrayList<>();
    ApiInfo apiInfo = new ApiInfo("Backend API", "API description for Innso Test", "6.6.6", "",
        contact, "", "", vext);

    Docket docket = new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo).pathMapping("/")
        .apiInfo(ApiInfo.DEFAULT).forCodeGeneration(true)
        .genericModelSubstitutes(ResponseEntity.class).ignoredParameterTypes(Pageable.class)
        .ignoredParameterTypes(java.sql.Date.class)
        .directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
        .directModelSubstitute(java.time.ZonedDateTime.class, Date.class)
        .directModelSubstitute(java.time.LocalDateTime.class, Date.class);

    return docket;
  }
}
