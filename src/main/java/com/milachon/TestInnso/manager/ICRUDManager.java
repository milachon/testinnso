package com.milachon.TestInnso.manager;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ICRUDManager<E> {
  public void deleteByUuid(String uuid);

  public Page<E> getList(Pageable properties);

  public List<E> getList();

  public void delete(E entity);

  public E save(E entity);

  public E update(String uuid, E entity);

  public E getByUuid(String uuid);

  public void saveList(List<E> entities);
}
