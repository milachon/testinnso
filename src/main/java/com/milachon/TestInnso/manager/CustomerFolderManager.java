package com.milachon.TestInnso.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.milachon.TestInnso.domain.model.CustomerFolder;
import com.milachon.TestInnso.domain.model.Message;
import com.milachon.TestInnso.repository.ICustomerFolderRepository;

@Service
public class CustomerFolderManager
    extends AbstractManager<ICustomerFolderRepository, CustomerFolder> {

  @Autowired
  MessageManager messageManager;

  public CustomerFolder createFromMessage(String messageUuid) {
    Message message = messageManager.getByUuid(messageUuid);

    CustomerFolder folder = new CustomerFolder();
    folder.setCustomerName(message.getAuthor());
    save(folder);

    message.setCustomerFolder(folder);
    message = messageManager.save(message);

    folder.getMessages().add(message);
    return save(folder);
  }

  public CustomerFolder addNewMessage(String uuid, Message message) {
    CustomerFolder folder = getByUuid(uuid);

    message.setCustomerFolder(folder);
    message = messageManager.save(message);

    folder.getMessages().add(message);
    return save(folder);
  }
}
