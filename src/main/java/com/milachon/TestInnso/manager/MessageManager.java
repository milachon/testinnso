package com.milachon.TestInnso.manager;

import org.springframework.stereotype.Service;
import com.milachon.TestInnso.domain.model.Message;
import com.milachon.TestInnso.repository.IMessageRepository;

@Service
public class MessageManager extends AbstractManager<IMessageRepository, Message> {
}
