package com.milachon.TestInnso.manager;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import com.milachon.TestInnso.domain.model.AbstractEntity;
import com.milachon.TestInnso.repository.IAbstractRepository;

public abstract class AbstractManager<R extends IAbstractRepository<E>, E extends AbstractEntity>
    implements ICRUDManager<E> {

  @Autowired
  protected R repo;

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void deleteByUuid(String uuid) {
    repo.deleteByUuid(uuid);
  }

  @Override
  public Page<E> getList(Pageable properties) {
    Page<E> page = repo.findAll(properties);
    return new PageImpl<>(page.getContent(), properties, page.getTotalElements());
  }

  @Override
  public List<E> getList() {
    List<E> list = new ArrayList<>();
    repo.findAll().forEach(list::add);

    return list;
  }

  @Override
  public void delete(E entity) {
    repo.delete(entity);
  }

  @Override
  public E save(E entity) {
    return repo.save(entity);
  }

  @Override
  public E update(String uuid, E newEntity) {
    E entity = getByUuid(uuid);
    entity.copyNonNullPropertiesFrom(newEntity);
    return save(entity);
  }

  @Override
  public E getByUuid(String uuid) {
    // TODO for all get in manager, don't throw exception but send back null
    return repo.findByUuid(uuid).orElse(null);
  }

  @Override
  public void saveList(List<E> entities) {
    repo.saveAll(entities);
  }
}
