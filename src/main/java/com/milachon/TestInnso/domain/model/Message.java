package com.milachon.TestInnso.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.milachon.TestInnso.domain.enumerator.Canal;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "message")
@Getter
@Setter
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "uuid")
public class Message extends AbstractEntity {

  @Column
  private String author;

  @Column
  private String content;

  @Enumerated(EnumType.STRING)
  @Column
  protected Canal canal;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "customer_folder_id")
  public CustomerFolder customerFolder;

  @Override
  public void copyNonNullPropertiesFrom(AbstractEntity orig) {

    Message origMessage = (Message) orig;

    super.copyNonNullPropertiesFrom(origMessage);

    if (origMessage.getCanal() != null) {
      setCanal(origMessage.getCanal());
    }
    if (origMessage.getAuthor() != null) {
      setAuthor(origMessage.getAuthor());
    }
    if (origMessage.getContent() != null) {
      setContent(origMessage.getContent());
    }
    if (origMessage.getCustomerFolder() != null) {
      setCustomerFolder(origMessage.getCustomerFolder());
    }
  }
}
