package com.milachon.TestInnso.domain.model;

import java.util.Collection;
import java.util.HashSet;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "customer_folder")
@Getter
@Setter
public class CustomerFolder extends AbstractEntity {

  @Column
  private String customerName;

  @Column
  private String reference;

  @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "customerFolder")
  private Collection<Message> messages = new HashSet<>();

  @Override
  public void copyNonNullPropertiesFrom(AbstractEntity orig) {

    CustomerFolder origFolder = (CustomerFolder) orig;

    super.copyNonNullPropertiesFrom(origFolder);

    if (origFolder.getCustomerName() != null) {
      setCustomerName(origFolder.getCustomerName());
    }
    if (origFolder.getReference() != null) {
      setReference(origFolder.getReference());
    }
  }
}
