package com.milachon.TestInnso.domain.enumerator;

import com.fasterxml.jackson.annotation.JsonCreator;

/** Canal used to send messages **/
public enum Canal {
  MAIL("MAIL"), SMS("SMS"), FACEBOOK("FACEBOOK"), TWITTER("TWITTER");

  private final String code;

  private Canal(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  @JsonCreator
  public static Canal fromText(String text) {
    if (text == null) {
      return null;
    }
    for (Canal sex : Canal.values()) {
      if (sex.getCode().equalsIgnoreCase(text)) {
        return sex;
      }
    }
    throw new IllegalArgumentException(
        text + " is an invalid " + Canal.class.getSimpleName() + " code.");
  }
}
