package com.milachon.TestInnso.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import com.milachon.TestInnso.domain.model.CustomerFolder;
import com.milachon.TestInnso.manager.CustomerFolderManager;

@AutoConfigureMockMvc
@WebMvcTest(CustomerFolderRestController.class)
public class CustomerFolderRestControllerTest
    extends AbstractRestControllerTest<CustomerFolder, CustomerFolderManager> {

  public CustomerFolderRestControllerTest() {
    super(CustomerFolder.class);
  }

  @Override
  protected CustomerFolder generateEntity() {
    CustomerFolder folder = new CustomerFolder();
    folder.setCustomerName("Toto Jackie");
    folder.setReference("AlphaBravo");
    folder.setUuid(UUID.randomUUID().toString());
    folder.setCreatedAt(new Date());
    folder.setMessages(new ArrayList<>());
    return folder;
  }

  @BeforeEach
  public void initData() {
    uri = "/customer_folders";
  }
}
