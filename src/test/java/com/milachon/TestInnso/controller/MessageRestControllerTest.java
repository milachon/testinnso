package com.milachon.TestInnso.controller;

import java.util.Date;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import com.milachon.TestInnso.domain.enumerator.Canal;
import com.milachon.TestInnso.domain.model.Message;
import com.milachon.TestInnso.manager.MessageManager;

@AutoConfigureMockMvc
@WebMvcTest(MessageRestController.class)
public class MessageRestControllerTest extends AbstractRestControllerTest<Message, MessageManager> {

  public MessageRestControllerTest() {
    super(Message.class);
  }

  @Override
  protected Message generateEntity() {
    Message mess = new Message();
    mess.setAuthor("Tata Michele");
    mess.setCanal(Canal.TWITTER);
    mess.setContent("Ceci est un contenu");
    mess.setUuid(UUID.randomUUID().toString());
    mess.setCreatedAt(new Date());
    return mess;
  }

  @BeforeEach
  public void initData() {
    uri = "/messages";
  }
}
