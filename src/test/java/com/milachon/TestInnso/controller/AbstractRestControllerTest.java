package com.milachon.TestInnso.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.milachon.TestInnso.domain.model.AbstractEntity;
import com.milachon.TestInnso.manager.AbstractManager;

@SuppressWarnings("rawtypes")
@AutoConfigureMockMvc
public abstract class AbstractRestControllerTest<E extends AbstractEntity, S extends AbstractManager> {

  final Class<E> typeEntityClass;

  public AbstractRestControllerTest(Class<E> typeEntityClass) {
    this.typeEntityClass = typeEntityClass;
  }

  protected String uri;

  @Autowired
  protected MockMvc mockMvc;

  @MockBean
  protected S service;

  protected ObjectMapper mapper = new ObjectMapper();

  protected static RandomStringGenerator stringGenerator =
      new RandomStringGenerator.Builder().build();

  protected E generateEntity() {
    return null;
  }

  // @Test
  // public void getEntityByUuidTest() throws Exception {
  // E entity = generateEntity();
  // given(service.getByUuid(entity.getUuid())).willReturn(entity);
  //
  // MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(uri + "/" + entity.getUuid()))
  // .andExpect(status().isOk()).andReturn();
  //
  // E response = mapper.readValue(result.getResponse().getContentAsString(), typeEntityClass);
  // assertThat(response).isEqualToComparingFieldByField(entity);
  // }
  //
  // @SuppressWarnings("unchecked")
  // @Test
  // public void createEntity() throws Exception {
  // E entity = generateEntity();
  // given(service.save(entity)).willReturn(entity);
  //
  // MvcResult result = mockMvc
  // .perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
  // .content(mapper.writeValueAsString(entity)))
  // .andExpect(status().isCreated()).andReturn();
  //
  // E response = mapper.readValue(result.getResponse().getContentAsString(), typeEntityClass);
  // assertThat(response).isEqualToComparingFieldByField(entity);
  // }
  //
  // @SuppressWarnings("unchecked")
  // @Test
  // public void updateEntity() throws Exception {
  // E entity = generateEntity();
  // uri += "/" + entity.getUuid();
  //
  // given(service.update(entity.getUuid(), entity)).willReturn(entity);
  //
  // String inputJson = mapper.writeValueAsString(entity);
  // MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put(uri)
  // .contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
  // .andExpect(status().isOk()).andReturn();
  //
  // E response = mapper.readValue(result.getResponse().getContentAsString(), typeEntityClass);
  // assertThat(response).isEqualToComparingFieldByField(entity);
  // }

  @Test
  public void deleteEntity() throws Exception {
    E entity = generateEntity();
    uri += "/" + entity.getUuid();

    mockMvc.perform(MockMvcRequestBuilders.delete(uri)).andExpect(status().isOk());
  }

  @Test
  public void getListEntity() throws Exception {
    List<E> entities = new ArrayList<E>();
    entities.add(generateEntity());
    entities.add(generateEntity());
    Page<E> page = new PageImpl<>(entities);

    Pageable pageable = PageRequest.of(0, 2);
    given(service.getList(pageable)).willReturn(page);

    MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(uri, pageable))
        .andExpect(status().isOk()).andReturn();

    assertEquals(0, pageable.getPageNumber());
    assertEquals(2, pageable.getPageSize());
  }
}
